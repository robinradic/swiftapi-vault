package org.radic.minecraft.swiftapi.vault.converters;

import org.radic.minecraft.swiftapi.vault.thrift.*;

public class BukkitConverter {

    public static Material material(org.bukkit.Material m) {


        return new Material(
                m.getMaxStackSize(),
                m.getMaxDurability(),
                m.isBlock(),
                m.isEdible(),
                m.isRecord(),
                m.isSolid(),
                m.isTransparent(),
                m.isFlammable(),
                m.isBurnable(),
                m.isOccluding(),
                m.hasGravity(),
                MaterialType.valueOf(m.toString())
        );

    }

    public static ItemInfo itemInfo(net.milkbowl.vault.item.ItemInfo itemInfo) {
        ItemInfo item = new ItemInfo();
        item.name = itemInfo.getName();
        item.material = material(itemInfo.getType());
        item.stackSize = itemInfo.getStackSize();
        item.edible = itemInfo.isEdible();
        item.block = itemInfo.isBlock();
        item.durable = itemInfo.isDurable();
        return item;
    }

    public static EconomyResponse economyResponse(net.milkbowl.vault.economy.EconomyResponse ec){
        EconomyResponse e = new EconomyResponse();
        e.amount = ec.amount;
        e.balance = ec.balance;
        e.errorMessage = ec.errorMessage;
        e.transactionSuccess = ec.transactionSuccess();
        e.type = EconomyResponseType.valueOf(ec.type.toString());
        return e;
    }

}
