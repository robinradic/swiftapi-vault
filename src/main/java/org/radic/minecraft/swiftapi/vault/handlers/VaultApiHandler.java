package org.radic.minecraft.swiftapi.vault.handlers;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse.ResponseType;
import net.milkbowl.vault.item.Items;
import net.milkbowl.vault.permission.Permission;
import org.apache.thrift.TException;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.phybros.minecraft.SwiftApiPlugin;
import org.phybros.minecraft.extensions.SwiftApiHandler;
import org.phybros.thrift.EAuthException;
import org.phybros.thrift.EDataException;
import org.phybros.thrift.ErrorCode;
import org.radic.minecraft.swiftapi.vault.VaultExtension;
import org.radic.minecraft.swiftapi.vault.converters.BukkitConverter;
import org.radic.minecraft.swiftapi.vault.thrift.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by radic on 7/21/14.
 */
public class VaultApiHandler extends SwiftApiHandler implements SwiftApiVault.Iface
{

    private OfflinePlayer getPlayerByName(String name) throws EDataException
    {
        for (OfflinePlayer op : VaultExtension.plugin.getServer().getOfflinePlayers())
        {
            if (op.getName().equals(name))
            {
                return op;
            }
        }
        SwiftApiPlugin.getInstance().getLogger().info("Player not found");
        EDataException e = new EDataException();
        e.code = ErrorCode.NOT_FOUND;
        e.errorMessage = String.format(
                SwiftApiPlugin.getInstance().getConfig().getString(
                        "errorMessages.playerNotFound"), name
        );
        throw e;
    }

    private Player getOnlinePlayerByName(String name) throws EDataException
    {
        for (Player op : VaultExtension.plugin.getServer().getOnlinePlayers())
        {
            if (op.getName().equals(name))
            {
                return op;
            }
        }
        SwiftApiPlugin.getInstance().getLogger().info("Player not found");
        EDataException e = new EDataException();
        e.code = ErrorCode.NOT_FOUND;
        e.errorMessage = String.format(
                SwiftApiPlugin.getInstance().getConfig().getString(
                        "errorMessages.playerNotFound"), name
        );
        throw e;
    }

    @Override
    public VaultPlayerData getPlayer(String authString, String playerName) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "getPlayer");

        VaultPlayerData playerData = new VaultPlayerData();
        OfflinePlayer player = getPlayerByName(playerName);
        Permission perms = VaultExtension.perms;
        Economy econ = VaultExtension.econ;
        List<World> worlds = VaultExtension.plugin.getServer().getWorlds();

        // banks
        if (econ.hasBankSupport())
        {
            ArrayList<VaultPlayerBank> playerBanks = new ArrayList<>();
            List<String> banks = econ.getBanks();
            for (String bank : banks)
            {
                playerBanks.add(new VaultPlayerBank(
                        bank,
                        econ.isBankOwner(bank, player).type.equals(ResponseType.SUCCESS),
                        econ.isBankMember(bank, player).type.equals(ResponseType.SUCCESS),
                        BukkitConverter.economyResponse(econ.bankBalance(bank))
                ));
            }
            playerData.setBanks(playerBanks);
        }


        HashMap<String, Double> playerBalance = new HashMap<>();
        playerBalance.put("global", econ.getBalance(player));
        for (World world : worlds)
        {
            playerBalance.put("global", econ.getBalance(player, world.getName()));
        }
        playerData.setBalance(playerBalance);


        // permissions/groups
        HashMap<String, String> playerPrimaryGroups = new HashMap<>();
        HashMap<String, List<String>> playerGroups = new HashMap<>();
        if (perms != null)
        {
            playerPrimaryGroups.put("global", perms.getPrimaryGroup(player.getPlayer()));
            playerGroups.put("global", new ArrayList<String>());
            for (String group : perms.getPlayerGroups(player.getPlayer()))
            {
                playerGroups.get("global").add(group);
            }

            for (World world : worlds)
            {
                playerPrimaryGroups.put(world.getName(), perms.getPrimaryGroup(world.getName(), player));

                playerGroups.put(world.getName(), new ArrayList<String>());
                for (String group : perms.getPlayerGroups(world.getName(), player.getPlayer()))
                {
                    playerGroups.get(world.getName()).add(group);
                }
            }
            playerData.setPrimaryGroups(playerPrimaryGroups);
            playerData.setGroups(playerGroups);
        }


        return playerData;
    }

    @Override
    public EconomyResponse depositPlayer(String authString, String playerName, double amount, String worldName) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "depositPlayer");
        OfflinePlayer player = getPlayerByName(playerName);
        return BukkitConverter.economyResponse(worldName == null ? VaultExtension.econ.depositPlayer(player, amount) : VaultExtension.econ.depositPlayer(player, worldName, amount));
    }

    @Override
    public EconomyResponse withdrawPlayer(String authString, String playerName, double amount, String worldName) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "withdrawPlayer");
        OfflinePlayer player = getPlayerByName(playerName);
        return BukkitConverter.economyResponse(worldName == null ? VaultExtension.econ.withdrawPlayer(player, amount) : VaultExtension.econ.withdrawPlayer(player, worldName, amount));
    }

    @Override
    public boolean createPlayerAccount(String authString, String playerName, String worldName) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "createPlayerAccount");
        OfflinePlayer player = getPlayerByName(playerName);
        return worldName == null ? VaultExtension.econ.createPlayerAccount(player) : VaultExtension.econ.createPlayerAccount(player, worldName);
    }


    @Override
    public HashMap<String, EconomyResponse> getBanks(String authString) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "getBanks");
        HashMap<String, EconomyResponse> banks = new HashMap<>();
        if (VaultExtension.econ.hasBankSupport())
        {
            for (String bank : VaultExtension.econ.getBanks())
            {
                banks.put(bank, BukkitConverter.economyResponse(VaultExtension.econ.bankBalance(bank)));
            }
        }
        return banks;
    }

    @Override
    public EconomyResponse createBank(String authString, String bankName, String playerName) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "createBank");
        OfflinePlayer player = getPlayerByName(playerName);
        return BukkitConverter.economyResponse(VaultExtension.econ.createBank(bankName, player));
    }

    @Override
    public EconomyResponse deleteBank(String authString, String bankName) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "deleteBank");
        return BukkitConverter.economyResponse(VaultExtension.econ.deleteBank(bankName));
    }

    @Override
    public EconomyResponse bankDeposit(String authString, String bankName, double amount) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "bankDeposit");
        return BukkitConverter.economyResponse(VaultExtension.econ.bankDeposit(bankName, amount));
    }

    @Override
    public EconomyResponse bankWidthraw(String authString, String bankName, double amount) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "bankWidthraw");
        return BukkitConverter.economyResponse(VaultExtension.econ.bankWithdraw(bankName, amount));
    }

    @Override
    public List<String> getGroups(String authString) throws EAuthException, EDataException, TException
    {

        ArrayList<String> groups = new ArrayList<String>();
        for (String group : VaultExtension.perms.getGroups())
        {
            groups.add(group);
        }

        return groups;
    }

    @Override
    public boolean playerHasPermission(String authString, String playerName, String permission, String worldName) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "playerHasPermission");
        Player player = getOnlinePlayerByName(playerName);
        return worldName == null ? VaultExtension.perms.playerHas(player, permission) : VaultExtension.perms.playerHas(worldName, player, permission);
    }

    @Override
    public boolean playerAddPermission(String authString, String playerName, String permission, String worldName) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "playerHasPermission");
        Player player = getOnlinePlayerByName(playerName);
        return worldName == null ? VaultExtension.perms.playerAdd(player, permission) : VaultExtension.perms.playerAdd(worldName, player, permission);
    }

    @Override
    public boolean playerRemovePermission(String authString, String playerName, String permission, String worldName) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "playerHasPermission");
        Player player = getOnlinePlayerByName(playerName);
        return worldName == null ? VaultExtension.perms.playerRemove(player, permission) : VaultExtension.perms.playerRemove(worldName, player, permission);
    }

    @Override
    public boolean groupHasPermission(String authString, String groupName, String permission, String worldName) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "playerHasPermission");
        return VaultExtension.perms.groupHas(worldName, groupName, permission);
    }

    @Override
    public boolean groupAddPermission(String authString, String groupName, String permission, String worldName) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "groupAddPermission");
        return VaultExtension.perms.groupAdd(worldName, groupName, permission);
    }

    @Override
    public boolean groupRemovePermission(String authString, String groupName, String permission, String worldName) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "groupRemovePermission");

        return VaultExtension.perms.groupRemove(worldName, groupName, permission);
    }

    @Override
    public boolean playerHasGroup(String authString, String playerName, String groupName, String worldName) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "playerHasGroup");
        return worldName == null ? VaultExtension.perms.playerInGroup(getOnlinePlayerByName(playerName), groupName) : VaultExtension.perms.playerInGroup(worldName, getPlayerByName(playerName), groupName);
    }

    @Override
    public boolean playerAddGroup(String authString, String playerName, String groupName, String worldName) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "playerAddGroup");
        return worldName == null ? VaultExtension.perms.playerAddGroup(getOnlinePlayerByName(playerName), groupName) : VaultExtension.perms.playerInGroup(worldName, getPlayerByName(playerName), groupName);
    }

    @Override
    public boolean playerRemoveGroup(String authString, String playerName, String groupName, String worldName) throws EAuthException, EDataException, TException
    {
        authenticateAndLog(authString, "playerRemoveGroup");
        return worldName == null ? VaultExtension.perms.playerRemoveGroup(getOnlinePlayerByName(playerName), groupName) : VaultExtension.perms.playerRemoveGroup(worldName, getPlayerByName(playerName), groupName);
    }


    @Override
    public EnabledServices getEnabledServices(String authString) throws EAuthException, EDataException, TException
    {
        return new EnabledServices(
                VaultExtension.econ != null,
                VaultExtension.chat != null,
                VaultExtension.perms != null,
                VaultExtension.econ != null || VaultExtension.econ.hasBankSupport()
        );
    }

    @Override
    public ItemInfo getItemById(String authString, int itemId) throws EAuthException, EDataException, TException
    {

        logCall("getItemById");
        authenticate(authString, "getItemById");
        net.milkbowl.vault.item.ItemInfo itemInfo = Items.itemByString(String.valueOf(itemId));
        return BukkitConverter.itemInfo(itemInfo);
    }

    @Override
    public ItemInfo getItemByName(String authString, String itemName) throws EAuthException, EDataException, TException
    {
        logCall("getItemByName");
        authenticate(authString, "getItemByName");
        net.milkbowl.vault.item.ItemInfo itemInfo = Items.itemByString(itemName);
        if (itemInfo != null)
        {
            return BukkitConverter.itemInfo(itemInfo);
        } else
        {
            return null;
        }
    }


}
