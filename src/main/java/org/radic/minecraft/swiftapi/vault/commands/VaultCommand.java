package org.radic.minecraft.swiftapi.vault.commands;
//Imports for the base command class.

import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.item.ItemInfo;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.phybros.minecraft.Api;
import org.phybros.minecraft.SwiftApiPlugin;
import org.phybros.minecraft.commands.ICommand;
import org.radic.minecraft.swiftapi.vault.VaultExtension;
import net.milkbowl.vault.item.Items;


//This class implements the Command Interface.
public class VaultCommand implements ICommand
{
    public static CommandSender lastSender;

    private void msg(String name, String message){
        Api.message(lastSender, VaultExtension.plugin, name, message);
    }

    private static Object eco(){
        return VaultExtension.econ;
    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args, SwiftApiPlugin plugin) {
        lastSender = sender;

        msg("args:length", "" + args.length);
        if(args.length > 0){
            for(int i = 0; i < args.length; i++) {
                msg("args:" + i, args[i]);
            }
        }


        if(args.length == 1) { // args[0] = vault
            boolean enabled = VaultExtension.econ.isEnabled();
            msg("econ:is-enabled", enabled ? "yes" : "no");
            if (enabled) {
                boolean bankSupport = VaultExtension.econ.hasBankSupport();
                msg("econ:has-bank-support", bankSupport ? "yes" : "no");
                if (bankSupport) {
                    msg("offline-players:toostring", plugin.getServer().getOfflinePlayers().toString());
                    for (OfflinePlayer player : plugin.getServer().getOfflinePlayers()) {
                        msg("offline-player:player:name", player.getName());


                        EconomyResponse response = VaultExtension.econ.depositPlayer(player, 44);
                        msg("economy:depositPlayer:economy-response:amount", response.amount + "");
                        msg("economy:depositPlayer:economy-response:amount", response.balance + "");
                        msg("economy:depositPlayer:economy-response:responsetype", response.type.name() + "");


                        response = VaultExtension.econ.createBank(player.getName() + "HisBank", player);
                        msg("economy:createbank:economy-response:amount", response.amount + "");
                        msg("economy:createbank:economy-response:amount", response.balance + "");
                        msg("economy:createbank:economy-response:responsetype", response.type.name() + "");
                    }
                }
            }
        } else {
            // args[0] = vault
            // args[1] = test
            if(args.length == 2){
                if(args[1].equals("test")){
                    // args[1] = item
                    // args[2] = id/name
                    ItemInfo item = Items.itemByType(Material.WOOD, (short) 1);
                    msg("iteminfo name", item.getName());
                }
            }
        }


        return true;
    }


}