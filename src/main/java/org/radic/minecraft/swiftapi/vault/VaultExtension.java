package org.radic.minecraft.swiftapi.vault;

import org.apache.thrift.TProcessor;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.phybros.minecraft.extensions.SwiftExtension;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.radic.minecraft.swiftapi.vault.commands.VaultCommand;
import org.radic.minecraft.swiftapi.vault.handlers.VaultApiHandler;
import org.radic.minecraft.swiftapi.vault.thrift.SwiftApiVault;


public class VaultExtension extends SwiftExtension {

    public static Economy econ = null;
    public static Permission perms = null;
    public static Chat chat = null;

    @Override
    public void register() {

        registerCommand("vault", new VaultCommand());
        registerApiHandler("org.radic.serverdata.vault.thrift.SwiftApiVault");
    }

    public void enable(){

        if (!setupEconomy() ) {
            log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        setupPermissions();
        setupChat();

    }

    public void disable() {
        log.info(String.format("[%s] Disabled Version %s", getDescription().getName(), getDescription().getVersion()));
    }

    @Override
    @SuppressWarnings("unchecked")
    public TProcessor getApiProcessor(String name) {
        if(name.equals("SwiftApiVault")) {
           return new SwiftApiVault.Processor(new VaultApiHandler());
        } else if(name.equals("SwiftApiVaultEconomy")) {
          //  return new SwiftApiVaultEconomy.Processor(new VaultEconomyApiHandler());
        }
        return null;
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> rsp = getServer().getServicesManager().getRegistration(Chat.class);
        chat = rsp.getProvider();
        return chat != null;
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }

}
