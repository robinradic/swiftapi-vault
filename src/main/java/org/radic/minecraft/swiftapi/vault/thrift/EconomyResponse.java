/**
 * Autogenerated by Thrift Compiler (0.9.2)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package org.radic.minecraft.swiftapi.vault.thrift;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import org.apache.thrift.async.AsyncMethodCallback;
import org.apache.thrift.server.AbstractNonblockingServer.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import javax.annotation.Generated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings({"cast", "rawtypes", "serial", "unchecked"})
@Generated(value = "Autogenerated by Thrift Compiler (0.9.2)", date = "2014-9-1")
public class EconomyResponse implements org.apache.thrift.TBase<EconomyResponse, EconomyResponse._Fields>, java.io.Serializable, Cloneable, Comparable<EconomyResponse> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("EconomyResponse");

  private static final org.apache.thrift.protocol.TField AMOUNT_FIELD_DESC = new org.apache.thrift.protocol.TField("amount", org.apache.thrift.protocol.TType.DOUBLE, (short)1);
  private static final org.apache.thrift.protocol.TField BALANCE_FIELD_DESC = new org.apache.thrift.protocol.TField("balance", org.apache.thrift.protocol.TType.DOUBLE, (short)2);
  private static final org.apache.thrift.protocol.TField TYPE_FIELD_DESC = new org.apache.thrift.protocol.TField("type", org.apache.thrift.protocol.TType.I32, (short)3);
  private static final org.apache.thrift.protocol.TField TRANSACTION_SUCCESS_FIELD_DESC = new org.apache.thrift.protocol.TField("transactionSuccess", org.apache.thrift.protocol.TType.BOOL, (short)4);
  private static final org.apache.thrift.protocol.TField ERROR_MESSAGE_FIELD_DESC = new org.apache.thrift.protocol.TField("errorMessage", org.apache.thrift.protocol.TType.STRING, (short)5);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new EconomyResponseStandardSchemeFactory());
    schemes.put(TupleScheme.class, new EconomyResponseTupleSchemeFactory());
  }

  public double amount; // required
  public double balance; // required
  /**
   * 
   * @see EconomyResponseType
   */
  public EconomyResponseType type; // required
  public boolean transactionSuccess; // required
  public String errorMessage; // optional

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    AMOUNT((short)1, "amount"),
    BALANCE((short)2, "balance"),
    /**
     * 
     * @see EconomyResponseType
     */
    TYPE((short)3, "type"),
    TRANSACTION_SUCCESS((short)4, "transactionSuccess"),
    ERROR_MESSAGE((short)5, "errorMessage");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // AMOUNT
          return AMOUNT;
        case 2: // BALANCE
          return BALANCE;
        case 3: // TYPE
          return TYPE;
        case 4: // TRANSACTION_SUCCESS
          return TRANSACTION_SUCCESS;
        case 5: // ERROR_MESSAGE
          return ERROR_MESSAGE;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __AMOUNT_ISSET_ID = 0;
  private static final int __BALANCE_ISSET_ID = 1;
  private static final int __TRANSACTIONSUCCESS_ISSET_ID = 2;
  private byte __isset_bitfield = 0;
  private static final _Fields optionals[] = {_Fields.ERROR_MESSAGE};
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.AMOUNT, new org.apache.thrift.meta_data.FieldMetaData("amount", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.DOUBLE)));
    tmpMap.put(_Fields.BALANCE, new org.apache.thrift.meta_data.FieldMetaData("balance", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.DOUBLE)));
    tmpMap.put(_Fields.TYPE, new org.apache.thrift.meta_data.FieldMetaData("type", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.EnumMetaData(org.apache.thrift.protocol.TType.ENUM, EconomyResponseType.class)));
    tmpMap.put(_Fields.TRANSACTION_SUCCESS, new org.apache.thrift.meta_data.FieldMetaData("transactionSuccess", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.BOOL)));
    tmpMap.put(_Fields.ERROR_MESSAGE, new org.apache.thrift.meta_data.FieldMetaData("errorMessage", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(EconomyResponse.class, metaDataMap);
  }

  public EconomyResponse() {
  }

  public EconomyResponse(
    double amount,
    double balance,
    EconomyResponseType type,
    boolean transactionSuccess)
  {
    this();
    this.amount = amount;
    setAmountIsSet(true);
    this.balance = balance;
    setBalanceIsSet(true);
    this.type = type;
    this.transactionSuccess = transactionSuccess;
    setTransactionSuccessIsSet(true);
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public EconomyResponse(EconomyResponse other) {
    __isset_bitfield = other.__isset_bitfield;
    this.amount = other.amount;
    this.balance = other.balance;
    if (other.isSetType()) {
      this.type = other.type;
    }
    this.transactionSuccess = other.transactionSuccess;
    if (other.isSetErrorMessage()) {
      this.errorMessage = other.errorMessage;
    }
  }

  public EconomyResponse deepCopy() {
    return new EconomyResponse(this);
  }

  @Override
  public void clear() {
    setAmountIsSet(false);
    this.amount = 0.0;
    setBalanceIsSet(false);
    this.balance = 0.0;
    this.type = null;
    setTransactionSuccessIsSet(false);
    this.transactionSuccess = false;
    this.errorMessage = null;
  }

  public double getAmount() {
    return this.amount;
  }

  public EconomyResponse setAmount(double amount) {
    this.amount = amount;
    setAmountIsSet(true);
    return this;
  }

  public void unsetAmount() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __AMOUNT_ISSET_ID);
  }

  /** Returns true if field amount is set (has been assigned a value) and false otherwise */
  public boolean isSetAmount() {
    return EncodingUtils.testBit(__isset_bitfield, __AMOUNT_ISSET_ID);
  }

  public void setAmountIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __AMOUNT_ISSET_ID, value);
  }

  public double getBalance() {
    return this.balance;
  }

  public EconomyResponse setBalance(double balance) {
    this.balance = balance;
    setBalanceIsSet(true);
    return this;
  }

  public void unsetBalance() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __BALANCE_ISSET_ID);
  }

  /** Returns true if field balance is set (has been assigned a value) and false otherwise */
  public boolean isSetBalance() {
    return EncodingUtils.testBit(__isset_bitfield, __BALANCE_ISSET_ID);
  }

  public void setBalanceIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __BALANCE_ISSET_ID, value);
  }

  /**
   * 
   * @see EconomyResponseType
   */
  public EconomyResponseType getType() {
    return this.type;
  }

  /**
   * 
   * @see EconomyResponseType
   */
  public EconomyResponse setType(EconomyResponseType type) {
    this.type = type;
    return this;
  }

  public void unsetType() {
    this.type = null;
  }

  /** Returns true if field type is set (has been assigned a value) and false otherwise */
  public boolean isSetType() {
    return this.type != null;
  }

  public void setTypeIsSet(boolean value) {
    if (!value) {
      this.type = null;
    }
  }

  public boolean isTransactionSuccess() {
    return this.transactionSuccess;
  }

  public EconomyResponse setTransactionSuccess(boolean transactionSuccess) {
    this.transactionSuccess = transactionSuccess;
    setTransactionSuccessIsSet(true);
    return this;
  }

  public void unsetTransactionSuccess() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __TRANSACTIONSUCCESS_ISSET_ID);
  }

  /** Returns true if field transactionSuccess is set (has been assigned a value) and false otherwise */
  public boolean isSetTransactionSuccess() {
    return EncodingUtils.testBit(__isset_bitfield, __TRANSACTIONSUCCESS_ISSET_ID);
  }

  public void setTransactionSuccessIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __TRANSACTIONSUCCESS_ISSET_ID, value);
  }

  public String getErrorMessage() {
    return this.errorMessage;
  }

  public EconomyResponse setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
    return this;
  }

  public void unsetErrorMessage() {
    this.errorMessage = null;
  }

  /** Returns true if field errorMessage is set (has been assigned a value) and false otherwise */
  public boolean isSetErrorMessage() {
    return this.errorMessage != null;
  }

  public void setErrorMessageIsSet(boolean value) {
    if (!value) {
      this.errorMessage = null;
    }
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case AMOUNT:
      if (value == null) {
        unsetAmount();
      } else {
        setAmount((Double)value);
      }
      break;

    case BALANCE:
      if (value == null) {
        unsetBalance();
      } else {
        setBalance((Double)value);
      }
      break;

    case TYPE:
      if (value == null) {
        unsetType();
      } else {
        setType((EconomyResponseType)value);
      }
      break;

    case TRANSACTION_SUCCESS:
      if (value == null) {
        unsetTransactionSuccess();
      } else {
        setTransactionSuccess((Boolean)value);
      }
      break;

    case ERROR_MESSAGE:
      if (value == null) {
        unsetErrorMessage();
      } else {
        setErrorMessage((String)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case AMOUNT:
      return Double.valueOf(getAmount());

    case BALANCE:
      return Double.valueOf(getBalance());

    case TYPE:
      return getType();

    case TRANSACTION_SUCCESS:
      return Boolean.valueOf(isTransactionSuccess());

    case ERROR_MESSAGE:
      return getErrorMessage();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case AMOUNT:
      return isSetAmount();
    case BALANCE:
      return isSetBalance();
    case TYPE:
      return isSetType();
    case TRANSACTION_SUCCESS:
      return isSetTransactionSuccess();
    case ERROR_MESSAGE:
      return isSetErrorMessage();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof EconomyResponse)
      return this.equals((EconomyResponse)that);
    return false;
  }

  public boolean equals(EconomyResponse that) {
    if (that == null)
      return false;

    boolean this_present_amount = true;
    boolean that_present_amount = true;
    if (this_present_amount || that_present_amount) {
      if (!(this_present_amount && that_present_amount))
        return false;
      if (this.amount != that.amount)
        return false;
    }

    boolean this_present_balance = true;
    boolean that_present_balance = true;
    if (this_present_balance || that_present_balance) {
      if (!(this_present_balance && that_present_balance))
        return false;
      if (this.balance != that.balance)
        return false;
    }

    boolean this_present_type = true && this.isSetType();
    boolean that_present_type = true && that.isSetType();
    if (this_present_type || that_present_type) {
      if (!(this_present_type && that_present_type))
        return false;
      if (!this.type.equals(that.type))
        return false;
    }

    boolean this_present_transactionSuccess = true;
    boolean that_present_transactionSuccess = true;
    if (this_present_transactionSuccess || that_present_transactionSuccess) {
      if (!(this_present_transactionSuccess && that_present_transactionSuccess))
        return false;
      if (this.transactionSuccess != that.transactionSuccess)
        return false;
    }

    boolean this_present_errorMessage = true && this.isSetErrorMessage();
    boolean that_present_errorMessage = true && that.isSetErrorMessage();
    if (this_present_errorMessage || that_present_errorMessage) {
      if (!(this_present_errorMessage && that_present_errorMessage))
        return false;
      if (!this.errorMessage.equals(that.errorMessage))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    List<Object> list = new ArrayList<Object>();

    boolean present_amount = true;
    list.add(present_amount);
    if (present_amount)
      list.add(amount);

    boolean present_balance = true;
    list.add(present_balance);
    if (present_balance)
      list.add(balance);

    boolean present_type = true && (isSetType());
    list.add(present_type);
    if (present_type)
      list.add(type.getValue());

    boolean present_transactionSuccess = true;
    list.add(present_transactionSuccess);
    if (present_transactionSuccess)
      list.add(transactionSuccess);

    boolean present_errorMessage = true && (isSetErrorMessage());
    list.add(present_errorMessage);
    if (present_errorMessage)
      list.add(errorMessage);

    return list.hashCode();
  }

  @Override
  public int compareTo(EconomyResponse other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = Boolean.valueOf(isSetAmount()).compareTo(other.isSetAmount());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetAmount()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.amount, other.amount);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetBalance()).compareTo(other.isSetBalance());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetBalance()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.balance, other.balance);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetType()).compareTo(other.isSetType());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetType()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.type, other.type);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetTransactionSuccess()).compareTo(other.isSetTransactionSuccess());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetTransactionSuccess()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.transactionSuccess, other.transactionSuccess);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetErrorMessage()).compareTo(other.isSetErrorMessage());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetErrorMessage()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.errorMessage, other.errorMessage);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("EconomyResponse(");
    boolean first = true;

    sb.append("amount:");
    sb.append(this.amount);
    first = false;
    if (!first) sb.append(", ");
    sb.append("balance:");
    sb.append(this.balance);
    first = false;
    if (!first) sb.append(", ");
    sb.append("type:");
    if (this.type == null) {
      sb.append("null");
    } else {
      sb.append(this.type);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("transactionSuccess:");
    sb.append(this.transactionSuccess);
    first = false;
    if (isSetErrorMessage()) {
      if (!first) sb.append(", ");
      sb.append("errorMessage:");
      if (this.errorMessage == null) {
        sb.append("null");
      } else {
        sb.append(this.errorMessage);
      }
      first = false;
    }
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // alas, we cannot check 'amount' because it's a primitive and you chose the non-beans generator.
    // alas, we cannot check 'balance' because it's a primitive and you chose the non-beans generator.
    if (type == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'type' was not present! Struct: " + toString());
    }
    // alas, we cannot check 'transactionSuccess' because it's a primitive and you chose the non-beans generator.
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class EconomyResponseStandardSchemeFactory implements SchemeFactory {
    public EconomyResponseStandardScheme getScheme() {
      return new EconomyResponseStandardScheme();
    }
  }

  private static class EconomyResponseStandardScheme extends StandardScheme<EconomyResponse> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, EconomyResponse struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // AMOUNT
            if (schemeField.type == org.apache.thrift.protocol.TType.DOUBLE) {
              struct.amount = iprot.readDouble();
              struct.setAmountIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // BALANCE
            if (schemeField.type == org.apache.thrift.protocol.TType.DOUBLE) {
              struct.balance = iprot.readDouble();
              struct.setBalanceIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // TYPE
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.type = org.radic.minecraft.swiftapi.vault.thrift.EconomyResponseType.findByValue(iprot.readI32());
              struct.setTypeIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 4: // TRANSACTION_SUCCESS
            if (schemeField.type == org.apache.thrift.protocol.TType.BOOL) {
              struct.transactionSuccess = iprot.readBool();
              struct.setTransactionSuccessIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 5: // ERROR_MESSAGE
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.errorMessage = iprot.readString();
              struct.setErrorMessageIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      if (!struct.isSetAmount()) {
        throw new org.apache.thrift.protocol.TProtocolException("Required field 'amount' was not found in serialized data! Struct: " + toString());
      }
      if (!struct.isSetBalance()) {
        throw new org.apache.thrift.protocol.TProtocolException("Required field 'balance' was not found in serialized data! Struct: " + toString());
      }
      if (!struct.isSetTransactionSuccess()) {
        throw new org.apache.thrift.protocol.TProtocolException("Required field 'transactionSuccess' was not found in serialized data! Struct: " + toString());
      }
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, EconomyResponse struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(AMOUNT_FIELD_DESC);
      oprot.writeDouble(struct.amount);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(BALANCE_FIELD_DESC);
      oprot.writeDouble(struct.balance);
      oprot.writeFieldEnd();
      if (struct.type != null) {
        oprot.writeFieldBegin(TYPE_FIELD_DESC);
        oprot.writeI32(struct.type.getValue());
        oprot.writeFieldEnd();
      }
      oprot.writeFieldBegin(TRANSACTION_SUCCESS_FIELD_DESC);
      oprot.writeBool(struct.transactionSuccess);
      oprot.writeFieldEnd();
      if (struct.errorMessage != null) {
        if (struct.isSetErrorMessage()) {
          oprot.writeFieldBegin(ERROR_MESSAGE_FIELD_DESC);
          oprot.writeString(struct.errorMessage);
          oprot.writeFieldEnd();
        }
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class EconomyResponseTupleSchemeFactory implements SchemeFactory {
    public EconomyResponseTupleScheme getScheme() {
      return new EconomyResponseTupleScheme();
    }
  }

  private static class EconomyResponseTupleScheme extends TupleScheme<EconomyResponse> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, EconomyResponse struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      oprot.writeDouble(struct.amount);
      oprot.writeDouble(struct.balance);
      oprot.writeI32(struct.type.getValue());
      oprot.writeBool(struct.transactionSuccess);
      BitSet optionals = new BitSet();
      if (struct.isSetErrorMessage()) {
        optionals.set(0);
      }
      oprot.writeBitSet(optionals, 1);
      if (struct.isSetErrorMessage()) {
        oprot.writeString(struct.errorMessage);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, EconomyResponse struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      struct.amount = iprot.readDouble();
      struct.setAmountIsSet(true);
      struct.balance = iprot.readDouble();
      struct.setBalanceIsSet(true);
      struct.type = org.radic.minecraft.swiftapi.vault.thrift.EconomyResponseType.findByValue(iprot.readI32());
      struct.setTypeIsSet(true);
      struct.transactionSuccess = iprot.readBool();
      struct.setTransactionSuccessIsSet(true);
      BitSet incoming = iprot.readBitSet(1);
      if (incoming.get(0)) {
        struct.errorMessage = iprot.readString();
        struct.setErrorMessageIsSet(true);
      }
    }
  }

}

