include '../SwiftApi/SwiftApi.thrift'

namespace java org.radic.minecraft.swiftapi.vault.thrift
namespace csharp org.radic.minecraft.swiftapi.vault.thrift
namespace php Radic.Minecraft.SwiftApi.Vault.Thrift

enum MaterialType {
    AIR, STONE, GRASS, DIRT, COBBLESTONE, WOOD, SAPLING, BEDROCK, WATER, STATIONARY_WATER, LAVA, STATIONARY_LAVA, SAND, GRAVEL, GOLD_ORE, IRON_ORE, COAL_ORE, LOG, LEAVES, SPONGE, GLASS, LAPIS_ORE, LAPIS_BLOCK, DISPENSER, SANDSTONE, NOTE_BLOCK, BED_BLOCK, POWERED_RAIL, DETECTOR_RAIL, PISTON_STICKY_BASE, WEB, LONG_GRASS, DEAD_BUSH, PISTON_BASE, PISTON_EXTENSION, WOOL, PISTON_MOVING_PIECE, YELLOW_FLOWER, RED_ROSE, BROWN_MUSHROOM, RED_MUSHROOM, GOLD_BLOCK, IRON_BLOCK, DOUBLE_STEP, STEP, BRICK, TNT, BOOKSHELF, MOSSY_COBBLESTONE, OBSIDIAN, TORCH, FIRE, MOB_SPAWNER, WOOD_STAIRS, CHEST, REDSTONE_WIRE, DIAMOND_ORE, DIAMOND_BLOCK, WORKBENCH, CROPS, SOIL, FURNACE, BURNING_FURNACE, SIGN_POST, WOODEN_DOOR, LADDER, RAILS, COBBLESTONE_STAIRS, WALL_SIGN, LEVER, STONE_PLATE, IRON_DOOR_BLOCK, WOOD_PLATE, REDSTONE_ORE, GLOWING_REDSTONE_ORE, REDSTONE_TORCH_OFF, REDSTONE_TORCH_ON, STONE_BUTTON, SNOW, ICE, SNOW_BLOCK, CACTUS, CLAY, SUGAR_CANE_BLOCK, JUKEBOX, FENCE, PUMPKIN, NETHERRACK, SOUL_SAND, GLOWSTONE, PORTAL, JACK_O_LANTERN, CAKE_BLOCK, DIODE_BLOCK_OFF, DIODE_BLOCK_ON
}


struct Material {
    1: required i32 maxStackSize
    2: required i32 maxDurability
    3: required bool block
    4: required bool edible
    5: required bool record
    6: required bool solid
    7: required bool transparent
    8: required bool flammable
    9: required bool burnable
    10: required bool occluding
    11: required bool usingGravity
    12: required MaterialType type
}

struct ItemInfo {
	1: string name,
	2: Material material,
	3: i32 stackSize,
	4: bool edible,
	5: bool block,
	6: bool durable,
	7: i32 subTypeId
}


struct VaultApiServices {
    1: required bool economyEnabled
    2: required bool chatEnabled
    3: required bool permissionsEnabled
    4: required bool bankSupportEnabled
}


service SwiftApiVault {
    VaultApiServices getEnabledServices(1:string authString)  throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
    ItemInfo getItemById(1:string authString, 2:i32 itemId) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex);
    ItemInfo getItemByName(1:string authString, 2:string name) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
}
