include '../SwiftApi/SwiftApi.thrift'

namespace java org.radic.minecraft.swiftapi.vault.thrift
namespace csharp org.radic.minecraft.swiftapi.vault.thrift
namespace php Radic.Minecraft.SwiftApi.Vault.Thrift


enum MaterialType {
    AIR, STONE, GRASS, DIRT, COBBLESTONE, WOOD, SAPLING, BEDROCK, WATER, STATIONARY_WATER, LAVA, STATIONARY_LAVA, SAND, GRAVEL, GOLD_ORE, IRON_ORE, COAL_ORE, LOG, LEAVES, SPONGE, GLASS, LAPIS_ORE, LAPIS_BLOCK, DISPENSER, SANDSTONE, NOTE_BLOCK, BED_BLOCK, POWERED_RAIL, DETECTOR_RAIL, PISTON_STICKY_BASE, WEB, LONG_GRASS, DEAD_BUSH, PISTON_BASE, PISTON_EXTENSION, WOOL, PISTON_MOVING_PIECE, YELLOW_FLOWER, RED_ROSE, BROWN_MUSHROOM, RED_MUSHROOM, GOLD_BLOCK, IRON_BLOCK, DOUBLE_STEP, STEP, BRICK, TNT, BOOKSHELF, MOSSY_COBBLESTONE, OBSIDIAN, TORCH, FIRE, MOB_SPAWNER, WOOD_STAIRS, CHEST, REDSTONE_WIRE, DIAMOND_ORE, DIAMOND_BLOCK, WORKBENCH, CROPS, SOIL, FURNACE, BURNING_FURNACE, SIGN_POST, WOODEN_DOOR, LADDER, RAILS, COBBLESTONE_STAIRS, WALL_SIGN, LEVER, STONE_PLATE, IRON_DOOR_BLOCK, WOOD_PLATE, REDSTONE_ORE, GLOWING_REDSTONE_ORE, REDSTONE_TORCH_OFF, REDSTONE_TORCH_ON, STONE_BUTTON, SNOW, ICE, SNOW_BLOCK, CACTUS, CLAY, SUGAR_CANE_BLOCK, JUKEBOX, FENCE, PUMPKIN, NETHERRACK, SOUL_SAND, GLOWSTONE, PORTAL, JACK_O_LANTERN, CAKE_BLOCK, DIODE_BLOCK_OFF, DIODE_BLOCK_ON
}

struct Material {
    1: required i32 maxStackSize
    2: required i32 maxDurability
    3: required bool block
    4: required bool edible
    5: required bool record
    6: required bool solid
    7: required bool transparent
    8: required bool flammable
    9: required bool burnable
    10: required bool occluding
    11: required bool usingGravity
    12: required MaterialType type
}

struct ItemInfo {
	1: string name,
	2: Material material,
	3: i32 stackSize,
	4: bool edible,
	5: bool block,
	6: bool durable,
	7: i32 subTypeId
}

struct EnabledServices {
    1: required bool economy
    2: required bool chat
    3: required bool permissions
    4: required bool bank
}


enum EconomicEntityType {
    PLAYER = 1,
    GROUP = 2
}

enum EconomyResponseType {
    SUCCESS = 1,
    FAILURE = 2,
    NOT_IMPLEMENTED = 3
}

struct EconomyResponse {
    1: required double amount,
    2: required double balance
    3: required EconomyResponseType type,
    4: required bool transactionSuccess,
    5: optional string errorMessage
}

struct VaultPlayerBank {
    1: string bankName,
    2: bool owner,
    3: bool member,
    4: EconomyResponse balance
}

struct VaultPlayerData {
    1: map<string, double> balance,
    2: list<VaultPlayerBank> banks,
    3: map<string, string> primaryGroups,
    4: map<string, list<string>> groups
}


service SwiftApiVault {
    # GLOBAL
    EnabledServices getEnabledServices(1:string authString)  throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
    VaultPlayerData getPlayer(1:string authString, 2:string playerName) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),


    # ITEMS
    ItemInfo getItemById(1:string authString, 2:i32 itemId) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex);
    ItemInfo getItemByName(1:string authString, 2:string name) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),

    /*
    worldname = null = global
     */

    # ECONOMY
    EconomyResponse depositPlayer(1:string authString, 2:string playerName, 3:double amount, 4:string worldName) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
    EconomyResponse withdrawPlayer(1:string authString, 2:string playerName, 3:double amount, 4:string worldName) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
    bool createPlayerAccount(1:string authString, 2:string playerName, 3:string worldName) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),


    map<string, EconomyResponse> getBanks(1:string authString) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
    EconomyResponse createBank(1:string authString, 2:string bankName, 3:string playerName) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
    EconomyResponse deleteBank(1:string authString, 2:string bankName) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
    EconomyResponse bankDeposit(1:string authString, 2:string bankName, 3:double amount) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
    EconomyResponse bankWidthraw(1:string authString, 2:string bankName, 3:double amount) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),

    # PERMISSIONS
    list<string> getGroups(1:string authString) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
    bool playerHasPermission(1:string authString, 2:string playerName, 3:string permission, 4:string worldName) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
    bool playerAddPermission(1:string authString, 2:string playerName, 3:string permission, 4:string worldName) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
    bool playerRemovePermission(1:string authString, 2:string playerName, 3:string permission, 4:string worldName) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),

    bool groupHasPermission(1:string authString, 2:string groupName, 3:string permission, 4:string worldName) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
    bool groupAddPermission(1:string authString, 2:string groupName, 3:string permission, 4:string worldName) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
    bool groupRemovePermission(1:string authString, 2:string groupName, 3:string permission, 4:string worldName) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),

    bool playerHasGroup(1:string authString, 2:string playerName, 3:string groupName, 4:string worldName) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
    bool playerAddGroup(1:string authString, 2:string playerName, 3:string groupName, 4:string worldName) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),
    bool playerRemoveGroup(1:string authString, 2:string playerName, 3:string groupName, 4:string worldName) throws (1:SwiftApi.EAuthException aex, 2:SwiftApi.EDataException dex),

}